# Cambio url
All'interno del file SensorSuite\SensorSuite\urls.py ci sono i vari metodi
```python
urlpatterns = [
    path('', views.index, name="index"),
    path('sensor_suite_core/', include('sensor_suite_core.urls')),
    path('logout/', views.user_logout, name="user_logout"),
    path('admin/', admin.site.urls),
    # Pagina dei rilevamenti
    path('sensors/', views.sensor_value, name="sensors"),
    # Url per XML
    path('get_xml/', views.get_xml, name="get_xml"),
    # Url per JSON
    path('get_json/', views.get_json, name="get_json"),
    # Url per CSV
    path('get_csv/', views.get_csv, name="get_csv"),
    path('update_machine_name/', views.update_machine_name, name="update_machine_name"),
    path('login/', views.user_login, name="user_login"),
]
```
basta cambiare il primo parametro del metodo path per far rispondere ad una URL diversa il relativo metodo ad esempio
```python
    # Url per XML
    path('xml/', views.get_xml, name="get_xml"),
]
```
