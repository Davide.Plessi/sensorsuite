# Dati in posizione fissa
Le posizioni sono mappate nel file SensorSuite\templates\sensor_suite_core\sensorValue.html a partire dal commento {# SENSORS DATA  #}
Il tag tr rappresenta la riga e il td la cella.
```html
{# SENSORS DATA  #}
    <tr>
        <td>
            <span class="bold">ser: </span>
            {% if 'ser' in data %}
                <span>{{ data.ser.value }}</span>
                <span>{{ data.ser.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
        <td>
            <span class="bold">ttl0: </span>
            {% if 'ttl0' in data %}
                <span>{{ data.ttl0.value }}</span>
                <span>{{ data.ttl0.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
        <td>
            <span class="bold">ttl1: </span>
            {% if 'ttl1' in data %}
                <span>{{ data.ttl1.value }}</span>
                <span>{{ data.ttl1.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
    </tr>
    <tr>
        <td>
            <span class="bold">ttl2: </span>
            {% if 'ttl2' in data %}
                <span>{{ data.ttl2.value }}</span>
                <span>{{ data.ttl2.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
        <td>
            <span class="bold">ttl3: </span>
            {% if 'ttl3' in data %}
                <span>{{ data.ttl3.value }}</span>
                <span>{{ data.ttl3.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
        <td>
            <span class="bold">ttl4: </span>
            {% if 'ttl4' in data %}
                <span>{{ data.ttl4.value }}</span>
                <span>{{ data.ttl4.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
    </tr>
    <tr>
        <td>
            <span class="bold">ttl5: </span>
            {% if 'ttl5' in data %}
                <span>{{ data.ttl5.value }}</span>
                <span>{{ data.ttl5.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
        <td>
            <span class="bold">ttl6: </span>
            {% if 'ttl6' in data %}
                <span>{{ data.ttl6.value }}</span>
                <span>{{ data.ttl6.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
        <td>
            <span class="bold">ttl7: </span>
            {% if 'ttl7' in data %}
                <span>{{ data.ttl7.value }}</span>
                <span>{{ data.ttl7.measurement_translation }}</span>
            {% else %}
                <span> - </span>
            {% endif %}
        </td>
    </tr>
```
Per mappare una posizione è necessario:
- sostituire il nome nell'if, ad esempio:
    {% if 'ttl7' in data %} -> {% if 'prova' in data %}
- sostituire la chiamata alla proprietà:
    ```html
        <span>{{ data.ttl7.value }}</span>
        <span>{{ data.ttl7.measurement_translation }}</span>
    ```
    in
    ```html
        <span>{{ data.prova.value }}</span>
        <span>{{ data.prova.measurement_translation }}</span>
    ```