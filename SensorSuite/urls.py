from django.contrib import admin
from django.urls import path, include
from sensor_suite_core import views

urlpatterns = [
    path('', views.index, name="index"),
    path('sensor_suite_core/', include('sensor_suite_core.urls')),
    path('logout/', views.user_logout, name="user_logout"),
    path('admin/', admin.site.urls),
    path('sensors/', views.sensor_value, name="sensors"),
    path('get_xml/', views.get_xml, name="get_xml"),
    path('get_json/', views.get_json, name="get_json"),
    path('get_csv/', views.get_csv, name="get_csv"),
    path('update_machine_name/', views.update_machine_name, name="update_machine_name"),
    path('login/', views.user_login, name="user_login"),
]