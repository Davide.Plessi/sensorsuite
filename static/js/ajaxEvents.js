$(function () {
  var insideAjax = false;
  $(document).ajaxStart(function () {
    insideAjax = true;
    setTimeout(function () {
      if (insideAjax)
        $('#overlay').show();
    }, 250);
  });

  $(document).ajaxStop(function () {
    insideAjax = false;
    $('#overlay').hide();
  });

  $(document).ajaxError(function (event, xhr, options, exc) {
    alert('Si è verificato un errore durante la comunicazione, riprovare!');
  });
});