from datetime import datetime

from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from sensor_suite_core.models import Registration, SensorData, SystemParameter
import json
import dicttoxml
import csv
import datetime
import pytz
import operator
from django.utils import timezone

utc = pytz.utc
local_timezone = pytz.timezone('Europe/Rome')

# Modifica per settare il numero di giorni per la conservazione delle registrazioni
data_maximum_age_days = 7

# Modifica per imposatre i minuti dopo i quali il sensore è considerato inattivo
time_gap_inactivity_minutes = 30


def index(request):
    return render(request, 'sensor_suite_core/index.html')


def user_login(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('index'))

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse('ACCOUNT NOT ACTIVE')
        else:
            print("Someone triedd to login and failed!")
            print('Username: {}, Password: {}'.format(username, password))
            return render(request, 'sensor_suite_core/login.html', {'error_message': 'Invalid username or password!'})
    else:
        return render(request, 'sensor_suite_core/login.html', {})


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


@login_required
def sensor_value(request):
    data = get_data()
    sensors = {}
    for sensor in data.sensordata_set.all():
        sensors[sensor.name] = {
            'value': sensor.value,
            'measurement_translation': sensor.get_measurement_translation_value()
        }

    working = data.date >= (
            timezone.now() - datetime.timedelta(
                minutes=time_gap_inactivity_minutes
            ))

    machine_name = get_machine_name()

    return render(
        request,
        'sensor_suite_core/sensorValue.html',
        {
            "working_class": (
                'valid'
                if working
                else 'invalid'
            ),
            "working_text": (
                'Ultimo aggiornamento '
                if working
                else 'Ultimo aggiornamento '
            ),
            "timestamp": data.date.astimezone(local_timezone).strftime('%Y-%m-%d %H:%M:%S'),
            "mac": data.mac,
            "ipp": data.ipp,
            "gw": data.gw,
            "sft_ver": data.sft_ver,
            "power_up_time": data.power_up_time,
            "wlan0": data.wlan0,
            "data": sensors,
            "machine_name": machine_name.value
        }
    )


@login_required
@csrf_exempt
def update_machine_name(request):
    if request.user.is_superuser:
        current_machine_name = get_machine_name()
        current_machine_name.value = request.POST["machineName"]
        current_machine_name.save()
        return JsonResponse({"success": True, "message": "Nome modificato correttamente"})
    else:
        return JsonResponse({"success": False, "message": "Utete non abilitato!"})


@csrf_exempt
def get_xml(request):
    try:
        days = int(request.GET["days"]) if "days" in request.GET.keys() else None
        data = get_data_straight_obj() \
            if not days \
            else get_data_straight_object_in_period(days)
    except Exception as exc:
        return JsonResponse({"success": False, "message": "Errore durante la lettura dei dati"})
    data_xml = dicttoxml.dicttoxml(data)
    return HttpResponse(data_xml, content_type="application/xml")


@csrf_exempt
def get_csv(request):
    days = int(request.GET["days"]) if "days" in request.GET.keys() else None
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="rilevamenti.csv"'
    writer = csv.writer(response, delimiter=';')

    if days:
        writer.writerow(['ID registrazione', 'Sensore', 'Valore'])
        data = get_data_straight_object_in_period(days)
        for key in data:
            for sub_key in data[key]:
                writer.writerow([key, sub_key, data[key][sub_key]])
    else:
        writer.writerow(['Grandezza', 'Valore'])
        data = get_data_straight_obj()
        for key in data:
            writer.writerow([key, data[key]])

    return response


@csrf_exempt
def get_json(request):
    try:
        days = int(request.GET["days"]) if "days" in request.GET.keys() else None
        data = get_data_straight_obj() \
            if not days \
            else get_data_straight_object_in_period(days)
    except Exception as exc:
        return JsonResponse({"success": False, "message": "Errore durante la lettura dei dati"})
    data_json = json.dumps(data)
    return HttpResponse(data_json, content_type="application/json")


def get_data_straight_obj(reg_id=None):
    return get_data(reg_id).get_straight_obj()


def get_data(reg_id=None):
    registration = Registration.objects.get(id=reg_id) if reg_id else None
    last_registration = registration if registration else Registration.objects.last()
    return last_registration


def get_data_straight_object_in_period(days):
    days = days if days else 1
    days = days if days > 0 else 1
    days = days if days < (data_maximum_age_days + 1) else data_maximum_age_days
    registrations = Registration.objects\
        .filter(date__gte=(datetime.datetime.now() - datetime.timedelta(days=days)))

    if not registrations:
        return None
    reg_index = 0
    result = dict()
    for registration in sorted(registrations, key=operator.attrgetter('date'), reverse=True):
        key_name = 'registration_{}'.format(registration.id)
        result[key_name] = registration.get_straight_obj()
        reg_index += 1

    return result


@csrf_exempt
def insert_data(request):
    try:
        delete_older()

        data = json.loads(request.body.decode('utf-8'))
        registration = Registration()
        time_stamp = (
            datetime.datetime.strptime(data['_timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')
            if '_timestamp' in data
            else datetime.datetime.now()
        )
        registration.date = time_stamp
        registration.mac = data['mac'] if 'mac' in data else None
        registration.ipp = data['ipp'] if 'ipp' in data else None
        registration.gw = data['gw'] if 'gw' in data else None
        registration.sft_ver = data['sft_ver'] if 'sft_ver' in data else None
        registration.power_up_time = data['power_up_time'] if 'power_up_time' in data else None
        registration.wlan0 = data['wlan0'] if 'wlan0' in data else None
        Registration.save(registration)
        for key in data:
            if key not in ['_timestamp', 'mac', 'ipp', 'gw', 'sft_ver', 'power_up_time', 'wlan0'] \
                    and key in data:
                sensor_data = SensorData()
                sensor_data.registration = registration
                sensor_data.name = key
                sensor_data.value = data[key]
                SensorData.save(sensor_data)
        return JsonResponse({"success": True})
    except Exception as exc:
        return JsonResponse({"success": False, "message": exc.__str__()})


def delete_older():
    Registration.objects \
        .filter(date__lte=(datetime.datetime.now() - datetime.timedelta(days=data_maximum_age_days))).delete()


def get_machine_name():
    value = SystemParameter.objects.filter(key='machine_name').first()
    if not value:
        new_sys_parameter = SystemParameter()
        new_sys_parameter.key = 'machine_name'
        new_sys_parameter.value = 'machine_name'
        new_sys_parameter.save()
        value = new_sys_parameter

    return value
