from django.contrib import admin
from sensor_suite_core.models import Registration, SensorData, MeasurementTranslation, SystemParameter

admin.site.register(Registration)
admin.site.register(SensorData)
admin.site.register(MeasurementTranslation)
admin.site.register(SystemParameter)