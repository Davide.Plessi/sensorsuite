import requests
import json
import datetime
import pytz

utc = pytz.UTC

payload = {
    "ser": 'ser',
    "ttl0": 'l0',
    "ttl1": 'l1',
    "ttl2": 'l2',
    "ttl3": 'l3',
    "ttl4": 'l4',
    "ttl5": 'l5',
    "ttl6": 'l6',
    "ttl7": 'l7',
    "mac": 'mac',
    "ipp": 'ipp',
    "gw": 'gw',
    "sft_ver": 'sft_ver',
    "power_up_time": 'power_up_time',
    "wlan0": 'wlan0',
    "_timestamp": datetime.datetime.now().isoformat()[:-3] + 'Z'
}
payload_json = json.dumps(payload)
url = 'http://localhost:8000/sensor_suite_core/insert_data/'
timeout = 15

headers = {'Content-Type': 'application/json'}
response = requests.post(url, data=payload_json, timeout=timeout, headers=headers)
print('Status code: {0}'.format(response.status_code))
print('Message: ' + response.text)

