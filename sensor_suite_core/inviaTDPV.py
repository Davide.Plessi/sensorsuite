#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
#import datetime
from datetime import datetime
#from time import sleep, time
import time
import socket
import fcntl
import struct
from cStringIO import StringIO
import pycurl
import json
import serial
import urlparse
from urlparse import urlsplit, urlunsplit
from datetime import timedelta
import argparse
import os
import xml.etree.ElementTree as ET
from datetime import datetime, tzinfo, timedelta
import sys
import os
from json import load
from urllib2 import urlopen
import logging
from logging.handlers import TimedRotatingFileHandler
import minimalmodbus





parser = argparse.ArgumentParser()
parser.add_argument("--first_time", help="Specifica se avvio dopo accensione/reboot.", action="store_true")
parser.add_argument("--no_terminal", help="Stampa su file invece che su terminale", action="store_true") #file.log_Set1
args = parser.parse_args()


#PORTA = "ttyUSB0"
PWD = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE = PWD + "/config_ser.conf"
D_TIMEOUT = 10
A_TIMEOUT = 24
GAUGES_TIMEOUT_CURL = 15 #minuti

#FILE.LOG
flog = None
if args.no_terminal:
    #flog = open(PWD+ "/" + datetime.strftime(datetime.now(), '%d-%m-%Y') + ".log", 'w')#usare sudo per aprire file.log #"/"=specifica dove deve essere scri$
    #flog = open(PWD+"/filelog" + ".log", 'w')
    flog = open(PWD+"/inviaTDPV" + ".log", 'w', 0)
    #flog = open(PWD+"/filelog-" + datetime.strftime(datetime.now(), '%d-%m-%Y') + ".log", 'w')
    #sys.stderr = sys.stdout = flog
    sys.stdout = flog #Nessuna BUFFERIZZAZIONE del file.log(flush), il file.log viene scritto immediatamente



def calc(data):
        crc_table=[0x0000,0xC0C1,0xC181,0x0140,0xC301,0x03C0,0x0280,0xC241,0xC601,0x06C0,0x0780,0xC741,0x0500,0xC5C1,0xC481,0x0440,0xCC01,0x0CC0,0x0D80,0xCD41,0x0F00,0xCFC1,0xCE81,0x0E40,0x0A00,0xCAC1,0xCB81,0x0B40,0xC901,0x09C0,0x0880,0xC841,0xD801,0x18C0,0x1980,0xD941,0x1B00,0xDBC1,0xDA81,0x1A40,0x1E00,0xDEC1,0xDF81,0x1F40,0xDD01,0x1DC0,0x1C80,0xDC41,0x1400,0xD4C1,0xD581,0x1540,0xD701,0x17C0,0x1680,0xD641,0xD201,0x12C0,0x1380,0xD341,0x1100,0xD1C1,0xD081,0x1040,0xF001,0x30C0,0x3180,0xF141,0x3300,0xF3C1,0xF281,0x3240,0x3600,0xF6C1,0xF781,0x3740,0xF501,0x35C0,0x3480,0xF441,0x3C00,0xFCC1,0xFD81,0x3D40,0xFF01,0x3FC0,0x3E80,0xFE41,0xFA01,0x3AC0,0x3B80,0xFB41,0x3900,0xF9C1,0xF881,0x3840,0x2800,0xE8C1,0xE981,0x2940,0xEB01,0x2BC0,0x2A80,0xEA41,0xEE01,0x2EC0,0x2F80,0xEF41,0x2D00,0xEDC1,0xEC81,0x2C40,0xE401,0x24C0,0x2580,0xE541,0x2700,0xE7C1,0xE681,0x2640,0x2200,0xE2C1,0xE381,0x2340,0xE101,0x21C0,0x2080,0xE041,0xA001,0x60C0,0x6180,0xA141,0x6300,0xA3C1,0xA281,0x6240,0x6600,0xA6C1,0xA781,0x6740,0xA501,0x65C0,0x6480,0xA441,0x6C00,0xACC1,0xAD81,0x6D40,0xAF01,0x6FC0,0x6E80,0xAE41,0xAA01,0x6AC0,0x6B80,0xAB41,0x6900,0xA9C1,0xA881,0x6840,0x7800,0xB8C1,0xB981,0x7940,0xBB01,0x7BC0,0x7A80,0xBA41,0xBE01,0x7EC0,0x7F80,0xBF41,0x7D00,0xBDC1,0xBC81,0x7C40,0xB401,0x74C0,0x7580,0xB541,0x7700,0xB7C1,0xB681,0x7640,0x7200,0xB2C1,0xB381,0x7340,0xB101,0x71C0,0x7080,0xB041,0x5000,0x90C1,0x9181,0x5140,0x9301,0x53C0,0x5280,0x9241,0x9601,0x56C0,0x5780,0x9741,0x5500,0x95C1,0x9481,0x5440,0x9C01,0x5CC0,0x5D80,0x9D41,0x5F00,0x9FC1,0x9E81,0x5E40,0x5A00,0x9AC1,0x9B81,0x5B40,0x9901,0x59C0,0x5880,0x9841,0x8801,0x48C0,0x4980,0x8941,0x4B00,0x8BC1,0x8A81,0x4A40,0x4E00,0x8EC1,0x8F81,0x4F40,0x8D01,0x4DC0,0x4C80,0x8C41,0x4400,0x84C1,0x8581,0x4540,0x8701,0x47C0,0x4680,0x8641,0x8201,0x42C0,0x4380,0x8341,0x4100,0x81C1,0x8081,0x4040]

        crc_hi=0xFF
        crc_lo=0xFF

        for w in data:
                index=crc_lo ^ ord(w)
                crc_val=crc_table[index]
                crc_temp=crc_val/256
                crc_val_low=crc_val-(crc_temp*256)
                crc_lo=crc_val_low ^ crc_hi
                crc_hi=crc_temp

        crc=crc_hi*256 +crc_lo
        return crc


def converti_bin(x,n=0):
    return format(x,'b').zfill(n)



def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ''.join(['%02x:' % ord(char) for char in info[18:24]])[:-1]





class Dispatcher:
    
    ##array-dizionario atto a contenere coppie data/ora (datetime.datetime) e valore pH per i valori di pH il cui invio non  e` riuscito immediatamente.
    #unsent_data_vals = {}

    ##dimensione del buffer circolare per unsent_data_vals
    UNSENT_DATA_VALS_BUFSIZ = 1000
                                  
    #Curl dataSent timeout DEVICE
    CURL_DATA_SENT_TIMEOUT = 5
    
    #Curl updateConfig timeout (json)
    CURL_UPDATE_CONFIG_TIMEOUT = 300
         
    #Start updateConfig timeout
    START_CONFIG_TIMEOUT = 0

    CURL_ALARM = 900
 
    #LISTA dei nomi dei "DATA" da controllare negli allarmi
    #SCALARS = ["t1", "t2", "ai", "ah", "h1", "h2", "h3", "h4", "ttl0", "ttl1", "ttl2", "ttl3", "ttl4", "ttl5", "ttl11", "i1", "i2", "i3", "i4", "i5", "ipp"]
    SCALARS = ["ttl0", "ttl1", "ttl2", "ttl4", "ttl5", "ttl6", "ttl7"]
	
    #Curl dataSent timeout GATEWAY
    CURL_DATA_SENT_TIMEOUT_GAT = 300
    
    #OFFSET TARATURA SONDA pH
    OFFSET = 0.00
         


    #Metodo per CALCOLO TIMESTAMP ISO format (Es. "timestamp": "2016-03-16T18:36:41.864Z")
    def tzname(self,**kwargs):
        return "UTC"
    
    def utcoffset(self, dt):
        return timedelta(0)       
        dt_ISO = datetime.utcnow().replace(tzinfo=simple_utc()).isoformat()
        

    def __init__(self, ser):
        self.ser = ser;
        self.devHeaders = {}
        
        ##array-dizionario atto a contenere coppie data/ora (datetime.datetime) e valore pH per i valori di pH il cui invio non  e` riuscito immediatamente.
        self.unsent_data_vals = {}
        self.stations = {}
        ##dimensione del buffer circolare per unsent_data_vals
        #self.UNSENT_DATA_VALS_BUFSIZ = 20
    
    def save_unsent(self, data):
        
        now = datetime.now()
        	
        if len(self.unsent_data_vals) == self.UNSENT_DATA_VALS_BUFSIZ: #and len(self.unsent_data_vals) > 0:
            popkey = sorted(self.unsent_data_vals.keys())[0]
            print("save_unsent: \033[1;31mlimite del buffer circolare (" + str(self.UNSENT_DATA_VALS_BUFSIZ) + ") raggiunto\033[0m: rimuovo il record " + datetime.strftime(popkey, '%d/%m/%Y %H:%M:%S') + " data " + str(self.unsent_data_vals[popkey]) )
            #logger.info("save_unsent: \033[1;31mlimite del buffer circolare (" + str(self.UNSENT_DATA_VALS_BUFSIZ) + ") raggiunto\033[0m: rimuovo il record " + datetime.strftime(popkey, '%d/%m/%Y %H:%M:%S') + " data " + str(self.unsent_data_vals[popkey]) )

            self.unsent_data_vals.pop(popkey)
    
        self.unsent_data_vals[now] = data #tot=lunghezza
        #print "array dizionario :", self.unsent_data_vals

    def add_new_station(self, station):
        if not self.stations.has_key(station):
            self.stations[station] = {
                'tstart': 0,     #timeout di start della sendData per ogni stazione
                'tcfg_start': 0, #timeout di start dell'updateConfig
                'gs': [],        #gs_info.json (lista)
                'evs': {},
                'scalars': {}    
            }
            for s in self.SCALARS:
                self.stations[station]['scalars'][s] = {
                    'talarm': 0,
                    'lastgood': False
                }
    	   
    def check_for_alarms(self, data):
        alarms = []
        station = data['ser'] 
        
        dt = datetime.now()
        sdt = datetime.strftime(dt, '%d-%m-%Y %H:%M:%S')       

        try:
            for ev in self.stations[station]['evs'].keys():
                expr = self.stations[station]['evs'][ev]['expr']
                scalar = self.stations[station]['evs'][ev]['scalar']
                action = self.stations[station]['evs'][ev]['action']
                #print '*' * 100
                #print "check di: " + scalar
                #print self.stations[station]['evs'][ev]['talarm']
                #print self.stations[station]['evs'][ev]['lastgood']
                try:    
                    if eval(expr):
                        current = time.time()
                        if (current - self.stations[station]['evs'][ev]['talarm'] > self.CURL_ALARM) or self.stations[station]['evs'][ev]['lastgood'] == True:
                            self.stations[station]['evs'][ev]['talarm'] = current
                            self.stations[station]['evs'][ev]['lastgood'] = False
                            self.sendAlarm(station, scalar, data[scalar], ev, expr, action, sdt) #Richiamo il metodo sendAlarm(curl POST)
                    else:
                        self.stations[station]['evs'][ev]['lastgood'] = True 
                except Exception as e:
                    print "Eval: chiave non valida o sintassi errata: ", e
                    #logger.info("Eval: chiave non valida o sintassi errata: ", e)
        except Exception as e:
            print "Errore in check alarm: ", e
            #logger.info("Errore in check alarm: ", e)


    def sendAlarm(self, station, scalar, value, evname, cond, action, sdt):
        #print "Stazione n. %d\nallarme -> %s\nEvento: %s\nAction: %s\nGrandezza: %s\nValore: %s\ntimestamp allarme: %s" % (station, evname, cond, action, scalar, str(value), str(sdt)) + "\n"
        
        ##***************************************sendAlarm PARSER********************************************************
        ##DEVICE ID=100000000000094 --> station*
        ##EVENT NAME=allarme_temp_bassa_acqua --> evname*
        ##ESPRESSIONE=data[t1]==20 --> cond*
        ##ACTION=warningAction (o parseAction,errorAction, ....) --> action*
        ##GRANDEZZA=t1 --> scalar*
        ##VALORE=20 --> value*
        ##TIMESTAMP ALLARME=14-04-2018 17:55:03 --> sdt*
        ##***************************************************************************************************************
 
        source = StringIO()
        success = True
       
        gatID = "8" + "{:0>14d}".format(station) #Creo variabile "devID" da passare nella URL
        action = str(action)                     #Creo variabile "action" da passare nella URL
        evname = str(evname)
        expression = str(cond)
        grandezza = str(scalar)
        value = str(value)

        
        URL0 = "https://iot.miosito.it/api/v1.0/store/app/MiaApp/object/devices." + gatID + "/action/" + action


        CURL_TIMEOUT = 2

        #dt = datetime.isoformat(datetime.utcnow())

        dtime = datetime.utcnow()
        dt=dtime.isoformat()[:-3]+'Z' #timestamp ISO-Format

        ##BODY curl POST ALARM corretto
        ##payload = {\"label\": \"info_t1_elevata\",\"value\": \"35\"}
        ##payload = '{\\"label\\": \\"' + str(evname) + '\\", \\"value\\": \\"' + str(value) + '\\" }'
        

        payload = '{\\"label\\": \\"' + str(evname) + '\\", \\"value\\": \\"' + str(value) + '\\"}'

        d = json.dumps(payload)

        curl = pycurl.Curl()
        curl.setopt(pycurl.URL, URL0)
        # Timeout trasferimento dati
        curl.setopt(pycurl.TIMEOUT, CURL_TIMEOUT)
        # Timeout connessione
        curl.setopt(pycurl.CONNECTTIMEOUT, CURL_TIMEOUT)
        curl.setopt(pycurl.WRITEFUNCTION, source.write)

        curl.setopt(pycurl.HTTPHEADER, ["Content-type:application/json",
                                        "api-key:HiiioopppptYVnRSKhiEuii"])

        curl.setopt(pycurl.POST, 1)
        curl.setopt(pycurl.POSTFIELDS, payload)

        #curl.perform()

        try:
            curl.perform()
            if curl.getinfo(curl.RESPONSE_CODE) <> 200:
                success = False
            #print source.getvalue()
        except pycurl.error, e:
            success = False
            #print e[1]


        dt = datetime.now()
        #dt = datetime.isoformat(datetime.utcnow())
        sdt = datetime.strftime(dt, '%d/%m/%Y %H:%M:%S')
        #sdt = datetime.strftime(dt, '%d/%m/%Y %H:%M:%S') sdt = str(datetime.isoformat(datetime.utcnow()))
        #sdt = str(dt)

        ret = ('\tStato: %d\tTempo totale: %f' % (curl.getinfo(curl.RESPONSE_CODE), curl.getinfo(curl.TOTAL_TIME)))
        if not success:
            print ("["+sdt+"] Invio allarme non riuscito --> gatewayID: %s%s" % (str(gatID), ret) + "\n") #"\n"= lascia 1 riga sotto la print
            #logger.info ("["+sdt+"] Invio allarme non riuscito --> gatewayID: %s%s" % (str(gatID), ret) + "\n")
            print d + "\n"
           
        else:
            print ("["+sdt+"] Inviato Allarme --> gatewayID: %s%s" % (str(gatID), ret)) #"\n"= 1 riga
            #logger.info ("["+sdt+"] Inviato Allarme --> gatewayID: %s%s" % (str(gatID), ret))
            print d + "\n"
            #logger.info (d + "\n")
            ##print ("expression=["+expression+"] ----> payload inviato=["+d+"]") + "\n"
            ##print "payload inviato=" + str(d) + "\n" 
            ##print "payload inviato:"
            ##print d + "\n"

            print "expression=" + str(cond) #+ "\n"
            print "EventName=" + str(evname) #+ "\n"
            print "Grandezza=" + str(scalar) #+ "\n"
            print "Action=" + str(action) #+ "\n"
            print "Valore=" + str(value) #+ "\n"
            print "Timestamp Allarme=" + sdt + "\n"

        curl.close()
        source.close()
        return success





    def parseEvents(self, station, jsconf):
        try:
            #self.stations[station]['evs'] = {}
            cases = jsconf['cases']
            alarms = cases.split('*')
            events = []
            for alarm in alarms:
                if alarm.count('/') == 1:
                    exp = alarm.split('/')
                    evt_name = exp[0]
                    events.append(evt_name)    
                    if exp[1].count(':') > 0:
                        tmp = exp[1].split(':')
                        action = tmp[1]
                        cond = tmp[0]
                    else:
                        action = ''
                        cond = exp[1]
                    cond = re.split('\s*([<>=]{1,2})\s*', cond)
                    expr = ''
                    scalar = '' 
                    for i in range(len(cond)):
                        if i%2 == 0:
                            try:
                                cond[i] = str(float(cond[i]))
                            except ValueError:
                                scalar = cond[i]
                                #scalars.append(scalar)
                                cond[i] = "data['" + cond[i] + "']"
                        expr += cond[i]
                    #self.stations[station]['evs'].append((evt_name, scalar, expr, action))
                    #print expr
                    new_evt = evt_name not in self.stations[station]['evs']
                    if new_evt:
                        self.stations[station]['evs'][evt_name] = {
                            'scalar': scalar,
                            'expr': expr,
                            'action': action,
                            'talarm': 0,
                            'lastgood': False
                        }
                    else:
                        self.stations[station]['evs'][evt_name]['scalar'] = scalar 
                        self.stations[station]['evs'][evt_name]['expr'] = expr 
                        self.stations[station]['evs'][evt_name]['action'] = action
                    #print new_evt
            for k in self.stations[station]['evs'].keys():
                if k not in events:
                    del self.stations[station]['evs'][k]
        except Exception as e:
            if len(self.stations[station]['evs']) > 0:
                self.stations[station]['evs'] = {}                
            print "Eccezione in parseEvents: ", e
            #logger.info("Eccezione in parseEvents: ", e)          
 
    def retry_unsent(self):
        print("FUNZIONE RETRY UNSENT")
        #logger.info("FUNZIONE RETRY UNSENT")
        progress = 0;
        count = 0
          
        ret = True
        total = len(self.unsent_data_vals)
        #print self.unsent_data_vals
        #logger.info(self.unsent_data_vals)
        for date in sorted(self.unsent_data_vals.keys()):
            progress += 1

            sdate = datetime.strftime(date, '%d/%m/%Y %H:%M:%S')
            #sdate = "---"
            sdate = self.unsent_data_vals[date]['_timestamp']
            print "retry_unsent: tento l'invio dei dati memorizzati in cache in data " + sdate + ": [" + str(progress) + "/" + str(total) + "]...\t" + "\n",
            #logger.info ("retry_unsent: tento l'invio dei dati memorizzati in cache in data " + sdate + ": [" + str(progress) + "/" + str(total) + "]...\t" + "\n",)

            data = self.unsent_data_vals[date]
            
            #print("********BUFFER_DATA: ".format(data))
            #print "***" + str( data['_timestamp'])
            #print "stampo data: " + str(data['ser'])
            success = self.sendData(data['ser'], data)
            if success:
                self.unsent_data_vals.pop(date)
		#del self.unsent_data_vals[date]
                count +=1
                print("[\033[1;32mOK\033[0m]")
                #logger.info("[\033[1;32mOK\033[0m]")             
            else:
                ret = False
                print("[\033[1;31mFALLITO\033[0m]")
                #logger.info("[\033[1;31mFALLITO\033[0m]")
                break
        #if count == total:
	    #self.unsent_data_vals.clear()
        return ret

    def updateConfig(self, station):
        
        current = time.time()
        #if current - self.START_CONFIG_TIMEOUT < self.CURL_UPDATE_CONFIG_TIMEOUT: 
        if current - self.stations[station]['tcfg_start'] < self.CURL_UPDATE_CONFIG_TIMEOUT:   
            return False, ''

        #self.START_CONFIG_TIMEOUT = current
        self.stations[station]['tcfg_start'] = current
                                                                
        source = StringIO()
        success = True
        
        js_cfg = {} #Nome JSON config_data
        gatID = "8" + "{:0>14d}".format(station)
        
        URL0 = "https://ot.miosito.it/api-plugin/v1.0/store/app/MiaApp/device/" + gatID + "/layout"      
  
        ctimeout = 2
        curl = pycurl.Curl()
        curl.setopt(pycurl.URL, URL0)
        
        curl.setopt(pycurl.HTTPHEADER, ["Content-Type:application/json",
                                        "api-key:joostrenmnlòòèpooijjmnkklhgY7"])

        curl.setopt(pycurl.WRITEFUNCTION, source.write)
        curl.setopt(pycurl.TIMEOUT, ctimeout)
        curl.setopt(pycurl.CONNECTTIMEOUT, ctimeout)
       
        try:
            curl.perform()
            if curl.getinfo(curl.RESPONSE_CODE) <> 200:
                success = False
            #print source.getvalue() #No print Url
        except pycurl.error, e:
            success = False
            #print e[1]

        dt = datetime.now()
        sdt = datetime.strftime(dt, '%d/%m/%Y %H:%M:%S')
        ret = ('\tStato: %d\tTempo totale: %f' % (curl.getinfo(curl.RESPONSE_CODE), curl.getinfo(curl.TOTAL_TIME)))
        if not success:
            print ("["+sdt+"] updateConfigData non riuscita --> stazione: %s%s" % (str(gatID), ret))# +"\n")
            #logger.info ("["+sdt+"] updateConfigData non riuscita --> stazione: %s%s" % (str(gatID), ret))
        else:
            #gs_config.json = {"gs_config_data":["t1","t2","ai","ah","h1"],
                              #"cases":["eventType1/t1>10*eventType2/i==0*EventType3/t2<4"],
                              #"id_device":"100000000000104"} 

            js_cfg = json.loads(source.getvalue())
            #if len(js_cfg['gauges_set']) > 0:
            self.stations[station]['gs'] = js_cfg['gauges_set']
            self.parseEvents(station, js_cfg)   
            print ("["+sdt+"] updateConfigData OK --> stazione: %s%s" % (str(gatID), ret))
            #logger.info ("["+sdt+"] updateConfigData OK --> stazione: %s%s" % (str(gatID), ret))
            #print "gs_info.json:"
            #print  "--- {} ---".format(json.dumps(gs_info))
            #print ("["+sdt+"] FirstContact OK --> " % (ret))

        curl.close()
        source.close()
        
        #print json "GS_INFO"
        #print '*' * 50
        #print js_cfg
        #print  "--- {} ---".format(json.loads(js_cfg))
        print  "--- {} ---".format(json.dumps(js_cfg)) + "\n"
        #logger.info ("--- {} ---".format(json.dumps(js_cfg)) + "\n")
        #print '*' * 50        
        #return success, js_cfg

    def sendToSensorSuite(self, payload_json, station):
        success = False
        gatID = "8" + "{:0>14d}".format(station)
        timeout = 2
        curl = pycurl.Curl()
        # URL di sensorSuite
        url_sensor_suite = 'http://localhost:80/sensor_suite_core/insert_data/'
        curl.setopt(pycurl.URL, url_sensor_suite)
        source = StringIO()
        curl.setopt(pycurl.WRITEFUNCTION, source.write)
        curl.setopt(pycurl.HTTPHEADER, ["Content-Type:application/json"])
        curl.setopt(pycurl.TIMEOUT, timeout)
        curl.setopt(pycurl.CONNECTTIMEOUT, timeout)
        curl.setopt(pycurl.POSTFIELDS, payload_json)
        try:
            curl.perform()
            if curl.getinfo(curl.RESPONSE_CODE) <> 200:
                success = False
        except pycurl.error, e:
            success = False
            print e[1]

        dt = datetime.now()
        #dt = datetime.isoformat(datetime.utcnow())
        sdt = datetime.strftime(dt, '%d/%m/%Y %H:%M:%S')
        #sdt = datetime.strftime(dt, '%d/%m/%Y %H:%M:%S')
        #sdt = str(datetime.isoformat(datetime.utcnow()))
        #sdt = str(dt)

        ret = ('\t Invio a SensorSuite \tStato: %d\tTempo totale: %f' % (curl.getinfo(curl.RESPONSE_CODE), curl.getinfo(curl.TOTAL_TIME)))
        if not success:
            print ("["+sdt+"] Invio dati SensorSuite non riuscito --> gatewayID: %s%s" % (str(gatID), ret) + "\n") #"\n"= lascia 1 riga sotto la print
            #logger.info ("["+sdt+"] Invio dati non riuscito --> gatewayID: %s%s" % (str(gatID), ret) + "\n")
            print d + "\n"
        else:
            print ("["+sdt+"] Invio dati SensorSuite OK --> gatewayID: %s%s" % (str(gatID), ret)) #"\n"= 1 riga
            #logger.info ("["+sdt+"] Invio dati OK --> gatewayID: %s%s" % (str(gatID), ret))
            ##print "payload inviato:"
            print d + "\n"
            #logger.info(d + "\n")

        curl.close()
        source.close()
        return success


    def sendData(self, station, data):
        #updateConfigStatus, gs_info = self.updateConfig(station), self.add_new_station(data['ser'])
        self.updateConfig(station)
        #Quando entra in senData fara la chiamata al metodo firstContact
        
              
        #if not updateConfigStatus : #Adesso avremo da controllore anche una nuova variabile l"updateConfigStatus"   
            #return 

        #print "--- {} ---".format(json.dumps(gs_info))
        

        source = StringIO()
        success = True
        #success = False
        param = ""
         
        gatID = "8" + "{:0>14d}".format(station)
        
  
        URL0 = "https://iot.miosito.it/api-plugin/v1.0/store/app/MiaApp/device/" + gatID + "/upload"       
 
        for k, v in data.items():
            param += (k + "=" + str(v) + ",") #replace parametri ricevuti sulla URL (invio dataCurl)

        payload = {}

        #if updateConfigStatus:
        if self.stations[station]['gs']:
            for data_cfg in self.stations[station]['gs']: #gs_info["gauges_set"]:
                payload[data_cfg] = str(data[data_cfg])
      
        else:
            payload = {
                #"ID1": {"t1": str(data['t1'] - 27)}}
                #"type": "STRING",
                "ser":  str(data['ser']), 
                "ttl0": str(data['ttl0']),
                "ttl1": str(data['ttl1']),
                "ttl2": str(data['ttl2']),
                "ttl3": str(data['ttl3']),
                "ttl4": str(data['ttl4']),
                "ttl5": str(data['ttl5']),
                "ttl6": str(data['ttl6']),
                "ttl7": str(data['ttl7']), 
                 "mac": str(data['mac']),
                #"eth": str(data['eth']),
                "ipp": str(data['ipp']),
                "gw": str(data['gw']),       
                "sft_ver": str(data['sft_ver']),
                "power_up_time": str(data['power_up_time']),
                "wlan0": str(data['wlan0'])
            }
        
        payload["_timestamp"] = str(data['_timestamp'])
        d = json.dumps(payload)

        
        ctimeout = 2
        curl = pycurl.Curl()
        curl.setopt(pycurl.URL, URL0)
        #curl.setopt(pycurl.HTTPHEADER, headers)
        #curl.setopt(pycurl.HTTPHEADER, HDATA)
        curl.setopt(pycurl.HTTPHEADER, ["Content-Type:application/json",
                                        "api-key:uiollgdbetgbnmllòòòò7"])

        curl.setopt(pycurl.WRITEFUNCTION, source.write)
        curl.setopt(pycurl.TIMEOUT, ctimeout)
        curl.setopt(pycurl.CONNECTTIMEOUT, ctimeout)
        curl.setopt(pycurl.POSTFIELDS, d)
        
        try:
            curl.perform()
            if curl.getinfo(curl.RESPONSE_CODE) <> 200:
                success = False
            #print source.getvalue()
        except pycurl.error, e:
            success = False
            print e[1]

        sendToSensorSuite(payload, station)

        dt = datetime.now()
        #dt = datetime.isoformat(datetime.utcnow())
        sdt = datetime.strftime(dt, '%d/%m/%Y %H:%M:%S')
        #sdt = datetime.strftime(dt, '%d/%m/%Y %H:%M:%S')
        #sdt = str(datetime.isoformat(datetime.utcnow()))
        #sdt = str(dt)

        ret = ('\tStato: %d\tTempo totale: %f' % (curl.getinfo(curl.RESPONSE_CODE), curl.getinfo(curl.TOTAL_TIME)))
        if not success:
            print ("["+sdt+"] Invio dati non riuscito --> gatewayID: %s%s" % (str(gatID), ret) + "\n") #"\n"= lascia 1 riga sotto la print
            #logger.info ("["+sdt+"] Invio dati non riuscito --> gatewayID: %s%s" % (str(gatID), ret) + "\n")
            print d + "\n"
        else:
            print ("["+sdt+"] Invio dati OK --> gatewayID: %s%s" % (str(gatID), ret)) #"\n"= 1 riga
            #logger.info ("["+sdt+"] Invio dati OK --> gatewayID: %s%s" % (str(gatID), ret))
            ##print "payload inviato:" 
            print d + "\n"
            #logger.info(d + "\n")

        curl.close()
        source.close()
        return success

    
    
    def discoverStations(self):
        #MAC ADDRES GATEWAY
        mac = getHwAddr('eth0')
       
                
        while True:
            ##self.ser.flushInput()
            ##self.ser.flushOutput()
            self.ser.write ("(MAC="+mac+"=4)")
            print "(MAC="+mac+"=4)"
            #logger.info("(MAC="+mac+"=4)")
            time.sleep(1)
            init = '' #inizializzo il primo registro di byte "init=x[1:2]" VUOTO
            x = self.ser.readline()
           
            if len(x) > 0:
                print x
                #logger.info(x)
                #print '*' * 100
                #************************BLOCCO TRY-EXCEPT*************************
                #Catturo le eccezioni nel caso in cui "init=x[1:2]" sia vuoto (nessun byte ricevuto). 
                #In  caso di eccezione (byte non ricevuti, byte deformati, ecc.) il programma python non terminerà e applicherà la sua "contro-misura"
                try:
                    #print x[1:2]
                    init=   x[1:2]
                except Exception as e:
                    #print "init: ", e
                    init = ''
                #*******************************************************************     

                #try:
                #    print x[2:3]    
                #    CH=     int(x[2:3], 16)
                #except Exception as e:
                #    print "ch: ", e
                #try:
                #    print x[3:5]
                #    VS_GAT= int(x[3:+5], 16)
                #except Exception as e:
                #    print "vs: ", e
                #try:
                #    print x[5:11]
                #    ACC=    int(x[5:11], 16)
                #except Exception as e:
                #    print "acc: ", e
            if init == "V":
                break


    def listenStations(self): #Ciclo lettura continua da seriale
        data = {}
        data_hex = {}
        #start = datetime.now()
        start = 0
        
        while True:
            data['ser'] = 201
            data['ttl0'] = instrument.read_register(40) #ID_DEPRESS
            data['ttl1'] = instrument.read_register(41) #ID_PRESS
            data['ttl2'] = instrument.read_register(42) #ID_TEMP_PTC
            data['ttl3'] = instrument.read_register(43) #ID_AN1
            data['ttl4'] = instrument.read_register(44) #ID_AN2
            data['ttl5'] = instrument.read_register(45) #ID_AN3
            data['ttl6'] = instrument.read_register(46) #ID_AN4
            data['ttl7'] = instrument.read_register(47) #ID_AN5


     
            ##gatewayIP
            try:
                gateway = os.popen("ip -4 route show default").read().split()
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                s.connect((gateway[2], 0))
                gw = gateway[2]
            except:
                #gw = "default gateway NULL"
                gw = "no_gw"
                print "no_gw"
                #logger.info("no_gw")
                    
            data['gw'] = gw
            pass


            ##wlan0 IP
            try:
                gw = os.popen("ip -4 route show default").read().split()
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                s.connect((gw[2], 0))
                wlan0 = s.getsockname()[0]
                gateway = gw[2]
                host = socket.gethostname()
            except:
                wlan0 = "no_wlan0"
                print "no_wlan0"
       
            data['wlan0'] = wlan0
            pass



            ##publicIP
            try:
                ipp = load(urlopen('https://api.ipify.org/?format=json'))['ip']
            except:
                #ipp = "NULL" #Inizializza ipp a NULL, quando la connessione internet non possibile calcolare l'ip pubblico
                                 #Si cattura cosi l'eccezione e si evita che lo script vada in traceback
                ipp = "no_ipp"
                print "no_ipp --> No Internet Connection"
                #logger.info("no_ipp --> No Internet Connection" + "\n")
                
            data['ipp'] = ipp                 
            pass ## --> consente lo sblocco dello script in caso di no_connection: alla verifica dell'eccezione "no_ipp" lo script "continua" e consente l'attivazione della funzione
                     ##"RETRY_UNSENT" (buffer)
 
            ##mac
            mac = getHwAddr('eth0')
            data['mac'] = mac
                
            ##sft_ver = vers. software gateway
            sft_ver = 2.0
            data['sft_ver'] = sft_ver

         

            ##Uptime = tempo dall'ultimo riavvio gateway
            with open('/proc/uptime', 'r') as f:
       
                uptime = float(f.readline().split()[0])
                    
                #power_up_time (calcolo in minuti)
                uptime_int = int(uptime)         ##Converto in intero
                power_up_time = uptime_int / 60  ##Calcolo del tempo in minuti: divido l'intero per 60
                data['power_up_time'] = power_up_time

               
          
   
            ##********************************************DEBUG:print "DATA" ricevuti dalla seriale (time.sleep=5 sec.)************************************************
            #print str(data) + "\n"
            ##*********************************************************************************************************************************************************


            ##********************************************TIMESTAMP in formato ISO = _timestamp (storico)*********************************************
            dtime = datetime.utcnow()
            dt = dtime.isoformat()[:-3]+'Z' 
            data['_timestamp'] = dt
            ##******************************************************************************************************************************************

                
            #Richiamo >> self.add_new_station(data['ser'])
            self.add_new_station(data['ser'])

            current = time.time() # Prendo l'orario attuale
                
            #st = data['ser']               
            delta = current - self.stations[data['ser']]['tstart'] #start # Calcolo il delta rispetto allo start time
                

            ##************************************sendDataDevice curl TIMEOUT + BUFFER*********************************************
            non_inviati = len(self.unsent_data_vals)
            #print "BUFFER =" + str(non_inviati)

            if delta > self.CURL_DATA_SENT_TIMEOUT:
                try_to_send = True 
                data_sent = False
                if non_inviati > 0:#>0 allora significa che abbiamo dei dati non inviati in buffer
                    try_to_send = self.retry_unsent()
                if try_to_send:
                    data_sent = self.sendData(data['ser'], data)
			        #data = {}
                if not data_sent:
                   self.save_unsent(data)
                   #data = {}
                   #start = current
                   #self.check_for_alarms(data) #richiamo metodo *check_for_alarms(data)*
                self.stations[data['ser']]['tstart'] = current
            self.check_for_alarms(data) #richiamo metodo "def check_for_alarm"
            data = {}
            #print buff
            #print '*' * 100
           

	    ##****************************************sendDataGateway curl TIMEOUT*********************************************************
            #current = time.time() # Prendo l'orario attuale

            #delta_sendDataGateway = current - start
            #delta_sendDataGateway_int = int(delta_sendDataGateway)
            #if delta_sendDataGateway_int > self.CURL_DATA_SENT_TIMEOUT_GAT:
                #self.sendDataGateway()
                #start = current
            ##********************************************************************************************************************
            
            ##Timestamp di interrogazione scheda MOODBUS
            time.sleep(1)
                
    #LOOP INFINITO su self
    def loop(self):
        while True:         
            #self.discoverStations();           
            self.listenStations();
            #self.add_new_station();
            #self.add_new_station(station);
            #self.updateConfig(station); 
            

 


##MODBUS PARAMETRI
instrument = minimalmodbus.Instrument('/dev/ttyS0', 1)
instrument.serial.baudrate = 4800   # Baud
instrument.serial.bytesize = 8
instrument.serial.parity   = serial.PARITY_NONE
instrument.serial.stopbits = 1
instrument.serial.timeout  = 2   # seconds



##SERIALE PARAMETRI            
#ser = serial.Serial(
    #port = '/dev/ttyUSB0',
    #baudrate = 9600,
    #parity = serial.PARITY_NONE,
    #stopbits = serial.STOPBITS_ONE,
    #bytesize = serial.EIGHTBITS,
    #timeout = 1
#)


##************************************************logger.info_LOG ROTANTI RotatingFileHandler*******************************************
#log_file = "/home/pi/pH/pH" #patch
##file = open(PWD+"/ph_" + time.strftime("%d-%m-%Y") + ".csv", 'a', 0)
#logger = logging.getLogger("Rotating Log")
#logger.setLevel(logging.INFO) #sostituire tutte le "print" X con logger.info(X)

##m=minuti, d=day; interval=intervallo rotazione log backupCount=n.file.log max da scrivere  
#handler = TimedRotatingFileHandler(log_file, when = "midnight", interval = 1, backupCount = 2)
##handler = TimedRotatingFileHandler(log_file, when = "d", interval = 1, backupCount = 5) #file.log giornaliero
                                                                                        
#logger.addHandler(handler)
##*************************************************************************************************************************************



disp = Dispatcher(instrument)
disp.loop()
#disp = inviaSer_curl_timeout()




