from django.urls import path
from sensor_suite_core import views

app_name = 'sensor_suite_core'

urlpatterns = [
    path('insert_data/', views.insert_data, name="insert_data"),
]
