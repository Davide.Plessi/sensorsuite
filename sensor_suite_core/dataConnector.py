import threading
import os

testing = True
full_data_script_path = os.path.dirname(__file__) + "\\inviaTDPV.py"


def start_data_script():
    command = "sudo python " + full_data_script_path
    os.system(command)


if not testing:
    t = threading.Thread(target=start_data_script)
    t.daemon = True
    t.start()
