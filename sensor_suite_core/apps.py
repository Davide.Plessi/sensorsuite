from django.apps import AppConfig


class SensorSuiteCoreConfig(AppConfig):
    name = 'sensor_suite_core'
