from django.db import models
import operator


class Registration(models.Model):
    date = models.DateTimeField(blank=False, null=False, auto_now_add=True)
    mac = models.CharField(max_length=500, blank=True, null=True)
    ipp = models.CharField(max_length=500, blank=True, null=True)
    gw = models.CharField(max_length=500, blank=True, null=True)
    sft_ver = models.CharField(max_length=500, blank=True, null=True)
    power_up_time = models.CharField(max_length=500, blank=True, null=True)
    wlan0 = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.date.isoformat()[:-3]+'Z ' + self.mac

    def get_straight_obj(self):
        data = {
            "_timestamp": self.date.isoformat()[:-3]+'Z',
            "wlan0": self.wlan0,
            "power_up_time": self.power_up_time,
            "sft_ver": self.sft_ver,
            "gw": self.gw,
            "ipp": self.ipp,
            "mac": self.mac,
        }
        sensors = sorted(list(self.sensordata_set.all()), key=operator.attrgetter('name'), reverse=False)
        for sensor in sensors:
            data[sensor.name] = sensor.value

        return data


class SensorData(models.Model):
    registration = models.ForeignKey(Registration, on_delete=models.CASCADE)
    name = models.CharField(max_length=60, blank=False, null=False)
    value = models.CharField(max_length=60, blank=False, null=False)

    def __str__(self):
        return self.registration.__str__() + ' - ' + self.name + ' : ' + self.value

    def get_measurement_translation_value(self):
        measurement_translation = MeasurementTranslation.objects.filter(sensor_name=self.name).first()
        return measurement_translation.value if measurement_translation else ''


class MeasurementTranslation(models.Model):
    sensor_name = models.CharField(max_length=60, blank=False, null=False)
    value = models.CharField(max_length=60, blank=False, null=False)

    def __str__(self):
        return self.sensor_name + ' : ' + self.value


class SystemParameter(models.Model):
    key = models.CharField(max_length=255, blank=False, null=False)
    value = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.key + ' : ' + self.value
