import requests
import os
import xml.dom.minidom

days = 7
savePath = "C:\\Progetti\\SensorSuite"
fileName = "output.xml"

url = 'http://localhost:8000/sensor_suite_core/get_xml/?days=' + days.__str__()
timeout = 15
headers = {'Content-Type': 'application/json'}
response = requests.get(url)
print('Status code: {0}'.format(response.status_code))
print('Message: ' + response.text)
if response.status_code == 200:
    result = xml.dom.minidom.parseString(response.text)
    if not os.path.exists(savePath):
        os.makedirs(savePath)
    with open(fileName, "w") as file:
        print(result.toprettyxml(), file=file)
