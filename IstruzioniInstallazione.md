# Installare apache2
```bash
sudo apt-get update
sudo apt-get install apache2 -y
```
# Installazione ambiente virtuale:
```
sudo pip3 install virtualenv
sudo mkdir /var/pyton3.5
sudo mkdir /var/python3.5/envs
sudo chmod -R 777 /var/python3.5/envs/SensorSuite
sudo virtualenv --python=/usr/bin/python3.5 /var/python3.5/envs/SensorSuite/
```
# Attivazione ambiente virtuale
```
cd /var/python3.5/envs/SensorSuite
source bin/activate
```

# Installazione pacchetti necessari
```
pip3 install dicttoxml
pip3 install Django
```

# Installazione mod wsgi
```
sudo apt-get install libapache2-mod-wsgi-py3
sudo a2enmod wsgi
```
# Configurazione di apache2

**Creare un file /etc/apache2/sites-available/sensorSuite.conf**
```
WSGIPythonHome /var/python3.5/envs/SensorSuite
WSGIPythonPath /var/www/SensorSuite

<VirtualHost *:8080>
	WSGIScriptAlias / /var/www/SensorSuite/SensorSuite/wsgi.py

	Alias /static /var/www/SensorSuite/static
	<Directory /var/www/SensorSuite/static>
		Require all granted
	</Directory>

	<Directory /var/www/SensorSuite/SensorSuite>
		<Files wsgi.py>
			Require all granted
		</Files>
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
**Aggiungere poi la direttiva Listen nel file di configurazione principale di apache**
```
Listen 8080
```

# Aggiornamento DB Locale
```
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py createsuperuser
```
# Configurazione permessi DB e cartella progetto
```
cd /var/www/SensorSuite
sudo chmod 664 db.sqlite3
sudo chmod 775 ./
sudo chown :www-data db.sqlite3
sudo chown :www-data ./
```

# Modifica testo grandezze
![GrigliaSensori](IstruzioniInstallazione/Modifica.jpg "Title")  
Per aggiornare il valore di display dell'unità di misura andare al link: http://127.0.0.1:8000/admin/sensor_suite_core/measurementtranslation/  
Si può aggiungere una nuova traduzione attraverso il bottone *ADD MEASUREMENT TRANSLATION*, Sensor name è il nome del sensore ad esempio ttl0
Value è la traduzione ad esempio Gradi.
Per modificarne una esistente clickare su una di quelle già presenti.

# Limite di giorni per la conservazione del dato
Nel file sensor_suite_core/view.py è presente la variabile: data_maximum_age_days = 7.  

# Minuti dopo i quali il sensore è considerato inattivo
Nel file sensor_suite_core/view.py è presente la variabile: time_gap_inactivity_minutes = 1.

# Aggiungere un nuovo utente
Al link http://127.0.0.1:8000/admin/auth/user/ clickando su ADD USER 

# Esportazione dati
## XML
È possibile scaricare i dati delle registrazioni in formato xml chiamando in GET il link .../sensor_suite_core/get_xml.  
Prende come parametro il numero di giorni: 1 -> dati dell'ultimo giorno ... 7 -> degli ultimi sette giorni. senza parametri prende l'ultima registrazione
Es .../sensor_suite_core/get_xml?days=7 prenderà tutti le registrazioni degli ultimi sette giorni
## JSON
Stessa cosa dell'xml ma il link è .../sensor_suite_core/get_json
## CSV
Stessa cosa dell'xml ma il link è .../sensor_suite_core/get_csv


